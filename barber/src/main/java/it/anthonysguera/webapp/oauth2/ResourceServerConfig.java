//package it.anthonysguera.webapp.oauth2;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
//import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
//import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
//import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
//import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
//
//@Configuration
//@EnableResourceServer
//public class ResourceServerConfig extends ResourceServerConfigurerAdapter
//{
//    private static final String RESOURCE_ID = "restservice";
//
//    private static final String[] USER_MATCHER = { "/**"};
//    private static final String[] ADMIN_MATCHER = { "/**" };
//
//    @Autowired
//    private ResourceServerProperties sso;
//
//    @Bean
//    public ResourceServerTokenServices myUserInfoTokenServices() {
//        return new CustomUserInfoTokenServices(sso.getUserInfoUri(), sso.getClientId());
//    }
//
//    @Override
//    public void configure(ResourceServerSecurityConfigurer resources)
//    {
//        resources.resourceId(RESOURCE_ID).stateless(false);
//    }
//
//    @Override
//    public void configure(HttpSecurity http) throws Exception
//    {
//        http
//                .anonymous().disable()
//                .requestMatchers().antMatchers("/**")
//                .and().authorizeRequests()
//                .antMatchers(USER_MATCHER).hasAnyRole("USER")
//                .antMatchers(ADMIN_MATCHER).hasAnyRole("ADMIN")
//                .and().exceptionHandling()
//                .accessDeniedHandler(new OAuth2AccessDeniedHandler());
//    }
//
//}