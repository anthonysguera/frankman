package it.anthonysguera.webapp.repository;

import it.anthonysguera.webapp.domain.Barbiere;
import org.springframework.data.repository.CrudRepository;

public interface BarbiereRepository extends CrudRepository<Barbiere, Long> {
}
