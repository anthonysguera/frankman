package it.anthonysguera.webapp.repository;

import it.anthonysguera.webapp.domain.Prenotazione;
import org.springframework.data.repository.CrudRepository;

public interface PrenotazioneRepository extends CrudRepository<Prenotazione, Long> {
}
