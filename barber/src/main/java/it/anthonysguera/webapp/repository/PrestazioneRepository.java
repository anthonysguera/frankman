package it.anthonysguera.webapp.repository;

import it.anthonysguera.webapp.domain.Prestazione;
import org.springframework.data.repository.CrudRepository;

public interface PrestazioneRepository extends CrudRepository<Prestazione, Long> {
}
