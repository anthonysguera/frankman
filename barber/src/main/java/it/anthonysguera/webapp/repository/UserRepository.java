package it.anthonysguera.webapp.repository;

import it.anthonysguera.webapp.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends CrudRepository<User, Long> {

    @Query(value = "select u.* from User u where email = (:email) ", nativeQuery = true)
    User findByEmail(@Param("email") String email);

}
