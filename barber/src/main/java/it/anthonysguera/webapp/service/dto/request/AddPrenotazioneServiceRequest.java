package it.anthonysguera.webapp.service.dto.request;

import lombok.Data;

@Data
public class AddPrenotazioneServiceRequest {

    private Long orario;
    private Long id_barbiere;
    private Long id_prestazione;
}
