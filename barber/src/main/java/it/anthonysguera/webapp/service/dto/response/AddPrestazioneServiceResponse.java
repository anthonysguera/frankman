package it.anthonysguera.webapp.service.dto.response;

import it.anthonysguera.webapp.domain.Prestazione;
import lombok.Data;

@Data
public class AddPrestazioneServiceResponse {

    private Prestazione prestazione;


}
