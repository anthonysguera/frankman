package it.anthonysguera.webapp.service;

import it.anthonysguera.webapp.service.dto.request.AddPrenotazioneServiceRequest;
import it.anthonysguera.webapp.service.dto.request.AddPrestazioneServiceRequest;
import it.anthonysguera.webapp.service.dto.request.FindUserServiceRequest;
import it.anthonysguera.webapp.service.dto.request.SaveUserServiceRequest;
import it.anthonysguera.webapp.service.dto.response.*;

public interface FrankmanService {

    SaveUserServiceResponse saveUser (SaveUserServiceRequest request) throws Exception;

    FindUserServiceResponse findByUsername (FindUserServiceRequest request) throws Exception;

    AddBarbiereServiceResponse addBarbiere (AddBarbiereServiceRequest request) throws Exception;

    AddPrenotazioneServiceResponse addPrenotazione (AddPrenotazioneServiceRequest request) throws  Exception;

    AddPrestazioneServiceResponse addPrestazione (AddPrestazioneServiceRequest request) throws Exception;





}
