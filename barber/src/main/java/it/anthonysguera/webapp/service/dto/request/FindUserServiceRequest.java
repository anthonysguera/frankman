package it.anthonysguera.webapp.service.dto.request;

import lombok.Data;

@Data
public class FindUserServiceRequest {

    private String email;
}
