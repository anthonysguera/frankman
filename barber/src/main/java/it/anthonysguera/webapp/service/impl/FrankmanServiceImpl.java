package it.anthonysguera.webapp.service.impl;

import it.anthonysguera.webapp.domain.Barbiere;
import it.anthonysguera.webapp.domain.Prenotazione;
import it.anthonysguera.webapp.domain.Prestazione;
import it.anthonysguera.webapp.domain.User;
import it.anthonysguera.webapp.repository.BarbiereRepository;
import it.anthonysguera.webapp.repository.PrenotazioneRepository;
import it.anthonysguera.webapp.repository.PrestazioneRepository;
import it.anthonysguera.webapp.repository.UserRepository;
import it.anthonysguera.webapp.service.AddBarbiereServiceRequest;
import it.anthonysguera.webapp.service.FrankmanService;
import it.anthonysguera.webapp.service.dto.request.AddPrenotazioneServiceRequest;
import it.anthonysguera.webapp.service.dto.request.AddPrestazioneServiceRequest;
import it.anthonysguera.webapp.service.dto.request.FindUserServiceRequest;
import it.anthonysguera.webapp.service.dto.request.SaveUserServiceRequest;
import it.anthonysguera.webapp.service.dto.response.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Calendar;

@Service("FrankmanServiceImpl")
@Transactional
public class FrankmanServiceImpl implements FrankmanService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BarbiereRepository barbiereRepository;

    @Autowired
    private PrenotazioneRepository prenotazioneRepository;

    @Autowired
    private PrestazioneRepository prestazioneRepository;

    private static final Logger logger = LoggerFactory.getLogger(FrankmanServiceImpl.class);

    @Override
    public SaveUserServiceResponse saveUser(SaveUserServiceRequest request) throws Exception {

        User user = new User();
        user.setNome(request.getNome());
        user.setCognome(request.getCognome());
        user.setEmail(request.getEmail());
        user.setPhone(request.getPhone());

        User user1 = userRepository.save(user);

        SaveUserServiceResponse saveUserServiceResponse = new SaveUserServiceResponse();
        saveUserServiceResponse.setUser(user1);

        logger.debug(String.valueOf(saveUserServiceResponse));

        return saveUserServiceResponse;

    }

    @Override
    public FindUserServiceResponse findByUsername(FindUserServiceRequest request) throws Exception {

        logger.debug(request.getEmail());
        User byEmail = userRepository.findByEmail(request.getEmail());

        FindUserServiceResponse findUserServiceResponse = new FindUserServiceResponse();

        findUserServiceResponse.setUser(byEmail);

        logger.debug(String.valueOf(findUserServiceResponse));

        return findUserServiceResponse;
    }

    @Override
    public AddBarbiereServiceResponse addBarbiere(AddBarbiereServiceRequest request) throws Exception {

        String nome = request.getNome();
        Barbiere barbiere = new Barbiere();
        barbiere.setNome(nome);
        Barbiere save = barbiereRepository.save(barbiere);

        AddBarbiereServiceResponse addBarbiereServiceResponse = new AddBarbiereServiceResponse();
        addBarbiereServiceResponse.setBarbiere(save);

        logger.debug(String.valueOf(addBarbiereServiceResponse));

        return addBarbiereServiceResponse;
    }

    @Override
    public AddPrenotazioneServiceResponse addPrenotazione(AddPrenotazioneServiceRequest request) throws Exception {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 10);
        cal.set(Calendar.MINUTE, 30);
        Long orario = cal.getTimeInMillis();

        Barbiere barbiere = barbiereRepository.findById(request.getId_barbiere())
                .orElse(null);

        Prestazione prestazione = prestazioneRepository.findById(request.getId_prestazione())
                .orElse(null);

        if (barbiere == null)
            throw new Exception();
        if (prestazione == null)
            throw new Exception();

        Prenotazione prenotazione = new Prenotazione();
        prenotazione.setBarbiere(barbiere);
        prenotazione.setPrestazione(prestazione);
        prenotazione.setOrario(orario);

        Prenotazione save = prenotazioneRepository.save(prenotazione);

        AddPrenotazioneServiceResponse addPrenotazioneServiceResponse = new AddPrenotazioneServiceResponse();
        addPrenotazioneServiceResponse.setPrenotazione(save);

        logger.debug(String.valueOf(addPrenotazioneServiceResponse));

        return addPrenotazioneServiceResponse;
    }

    @Override
    public AddPrestazioneServiceResponse addPrestazione(AddPrestazioneServiceRequest request) throws Exception {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MINUTE, 30);
        Long tempo_prestazione = cal.getTimeInMillis();

        String desc = request.getDescrizione();

        Prestazione prestazione = new Prestazione();
        prestazione.setDescrizione(desc);
        prestazione.setDurata(tempo_prestazione);

        Prestazione save = prestazioneRepository.save(prestazione);

        AddPrestazioneServiceResponse addPrestazioneServiceResponse = new AddPrestazioneServiceResponse();
        addPrestazioneServiceResponse.setPrestazione(save);

        logger.debug(String.valueOf(addPrestazioneServiceResponse));

        return addPrestazioneServiceResponse;
    }
}
