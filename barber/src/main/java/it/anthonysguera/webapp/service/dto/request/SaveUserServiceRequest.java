package it.anthonysguera.webapp.service.dto.request;

import lombok.Data;

@Data
public class SaveUserServiceRequest {

    private String nome;
    private String cognome;
    private String email;
    private String phone;

}
