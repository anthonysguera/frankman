package it.anthonysguera.webapp.service.dto.response;

import it.anthonysguera.webapp.domain.Barbiere;
import lombok.Data;

@Data
public class AddBarbiereServiceResponse {

    private Barbiere barbiere;

}
