package it.anthonysguera.webapp.service.dto.request;

import it.anthonysguera.webapp.domain.Prenotazione;
import lombok.Data;

import java.util.Set;

@Data
public class AddPrestazioneServiceRequest {

    private String descrizione;
    private Long durata;
}
