package it.anthonysguera.webapp.service;

import lombok.Data;

@Data
public class AddBarbiereServiceRequest {

    private String nome;
}
