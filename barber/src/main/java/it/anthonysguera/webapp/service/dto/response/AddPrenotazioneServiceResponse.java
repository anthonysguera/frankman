package it.anthonysguera.webapp.service.dto.response;

import it.anthonysguera.webapp.domain.Prenotazione;
import lombok.Data;

@Data
public class AddPrenotazioneServiceResponse {

    private Prenotazione prenotazione;


}
