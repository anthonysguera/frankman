package it.anthonysguera.webapp.service.dto.response;

import it.anthonysguera.webapp.domain.User;
import lombok.Data;

@Data
public class FindUserServiceResponse {

    private User user;

}
