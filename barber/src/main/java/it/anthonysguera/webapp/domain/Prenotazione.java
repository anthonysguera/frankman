package it.anthonysguera.webapp.domain;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@Table(name = "Prenotazione")
public class Prenotazione {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "data")
    private Timestamp data;

    @Column(name = "orario")
    private Long orario;

    @Column(name="is_done")
    private Boolean isDone;

    @ManyToOne
    @JoinColumn
    private Barbiere barbiere;

    @ManyToOne
    @JoinColumn
    private Prestazione prestazione;

}
