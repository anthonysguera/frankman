package it.anthonysguera.webapp.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Prestazione")
@Data
public class Prestazione {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "descizione")
    private String descrizione;

    @Column(name = "durata")
    private Long durata;

    @OneToMany(fetch = FetchType.LAZY)
    private Set<Prenotazione> prenotazioni;


}
