package it.anthonysguera.webapp.controller.dto.request;

import lombok.Data;

@Data
public class SaveUserControllerRequest {
    private String nome;
    private String cognome;
    private String email;
    private String phone;

}
