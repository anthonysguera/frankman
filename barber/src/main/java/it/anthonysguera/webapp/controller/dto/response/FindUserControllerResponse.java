package it.anthonysguera.webapp.controller.dto.response;

import it.anthonysguera.webapp.domain.User;
import lombok.Data;

@Data
public class FindUserControllerResponse {

    private User user;

}
