package it.anthonysguera.webapp.controller;

import it.anthonysguera.webapp.controller.dto.request.PopulateControllerRequest;
import it.anthonysguera.webapp.controller.dto.request.SaveUserControllerRequest;
import it.anthonysguera.webapp.controller.dto.response.FindUserControllerResponse;
import it.anthonysguera.webapp.controller.dto.response.PopulateControllerResponse;
import it.anthonysguera.webapp.controller.dto.response.SaveUserControllerResponse;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


public interface FrankmanController {

    @RequestMapping(value = "/user/save", method = RequestMethod.POST)
    ResponseEntity<SaveUserControllerResponse> saveUser (@RequestBody SaveUserControllerRequest request) throws Exception;

    @RequestMapping(value = "/user/cerca/{username}", method = RequestMethod.GET)
    ResponseEntity<FindUserControllerResponse> findUser (@PathVariable("username") String username) throws Exception;

    @RequestMapping(value = "/populate", method = RequestMethod.POST)
    ResponseEntity<PopulateControllerResponse> populateDb(@RequestBody PopulateControllerRequest request) throws Exception;

}
