package it.anthonysguera.webapp.controller.dto.request;

import lombok.Data;

@Data
public class FindUserControllerRequest {

    private String email;

}
