package it.anthonysguera.webapp.controller.impl;

import it.anthonysguera.webapp.controller.FrankmanController;
import it.anthonysguera.webapp.controller.dto.request.PopulateControllerRequest;
import it.anthonysguera.webapp.controller.dto.request.SaveUserControllerRequest;
import it.anthonysguera.webapp.controller.dto.response.FindUserControllerResponse;
import it.anthonysguera.webapp.controller.dto.response.PopulateControllerResponse;
import it.anthonysguera.webapp.controller.dto.response.SaveUserControllerResponse;
import it.anthonysguera.webapp.facade.dto.request.FindUserFacadeRequest;
import it.anthonysguera.webapp.facade.dto.request.PopulateFacadeRequest;
import it.anthonysguera.webapp.facade.impl.FrankmanFacadeImpl;
import it.anthonysguera.webapp.facade.dto.request.SaveUserFacadeRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController("FrankmanControllerImpl")
public class FrankmanControllerImpl implements FrankmanController {

    @Autowired
    FrankmanFacadeImpl frankmanFacade;


    @Override
    @ResponseBody
    public ResponseEntity<SaveUserControllerResponse> saveUser(@RequestBody SaveUserControllerRequest request) throws Exception {

        SaveUserFacadeRequest userFacadeRequest = new SaveUserFacadeRequest();
        BeanUtils.copyProperties(request, userFacadeRequest);
//        SaveUserFacadeResponse save = userFacade.save(userFacadeRequest);

        SaveUserControllerResponse saveUserControllerResponse = new SaveUserControllerResponse();
//        BeanUtils.copyProperties(save, saveUserControllerResponse);

        return new ResponseEntity<>(saveUserControllerResponse, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<FindUserControllerResponse> findUser(@PathVariable String username) throws Exception {
        FindUserFacadeRequest userFacadeRequest = new FindUserFacadeRequest();
        userFacadeRequest.setEmail(username);
//        FindUserFacadeResponse findUserFacadeResponse = userFacade.findByUsername(userFacadeRequest);

        FindUserControllerResponse findUserControllerResponse = new FindUserControllerResponse();
//        BeanUtils.copyProperties(findUserFacadeResponse, findUserControllerResponse);

        return new ResponseEntity<>(findUserControllerResponse, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<PopulateControllerResponse> populateDb(PopulateControllerRequest request) throws Exception {

        PopulateFacadeRequest populateFacadeRequest = new PopulateFacadeRequest();
        frankmanFacade.populateDb(populateFacadeRequest);
        PopulateControllerResponse populateControllerResponse = new PopulateControllerResponse();

        return new ResponseEntity<>(populateControllerResponse, HttpStatus.OK);
    }


}
