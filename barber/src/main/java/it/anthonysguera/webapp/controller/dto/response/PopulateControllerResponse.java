package it.anthonysguera.webapp.controller.dto.response;

import lombok.Data;

@Data
public class PopulateControllerResponse {

    private String done = "OK";

}
