package it.anthonysguera.webapp.facade.impl;


import it.anthonysguera.webapp.facade.FrankmanFacade;
import it.anthonysguera.webapp.facade.dto.request.FindUserFacadeRequest;
import it.anthonysguera.webapp.facade.dto.request.PopulateFacadeRequest;
import it.anthonysguera.webapp.facade.dto.request.SaveUserFacadeRequest;
import it.anthonysguera.webapp.facade.dto.response.FindUserFacadeResponse;
import it.anthonysguera.webapp.facade.dto.response.PopulateFacadeResponse;
import it.anthonysguera.webapp.facade.dto.response.SaveUserFacadeResponse;
import it.anthonysguera.webapp.service.AddBarbiereServiceRequest;
import it.anthonysguera.webapp.service.dto.request.AddPrenotazioneServiceRequest;
import it.anthonysguera.webapp.service.dto.request.AddPrestazioneServiceRequest;
import it.anthonysguera.webapp.service.dto.request.FindUserServiceRequest;
import it.anthonysguera.webapp.service.dto.request.SaveUserServiceRequest;
import it.anthonysguera.webapp.service.dto.response.AddBarbiereServiceResponse;
import it.anthonysguera.webapp.service.dto.response.AddPrestazioneServiceResponse;
import it.anthonysguera.webapp.service.dto.response.FindUserServiceResponse;
import it.anthonysguera.webapp.service.dto.response.SaveUserServiceResponse;
import it.anthonysguera.webapp.service.impl.FrankmanServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("FrankmanFacadeImpl")
public class FrankmanFacadeImpl implements FrankmanFacade {

    @Autowired
    FrankmanServiceImpl frankmanService;

    @Override
    public SaveUserFacadeResponse save(SaveUserFacadeRequest request) throws Exception {

        SaveUserServiceRequest serviceRequest = new SaveUserServiceRequest();
        BeanUtils.copyProperties(request, serviceRequest);
        SaveUserServiceResponse saveUserServiceResponse = frankmanService.saveUser(serviceRequest);

        SaveUserFacadeResponse facadeResponse = new SaveUserFacadeResponse();
        facadeResponse.setUser(saveUserServiceResponse.getUser());

        return facadeResponse;
    }

    @Override
    public FindUserFacadeResponse findByUsername(FindUserFacadeRequest request) throws Exception {

        FindUserServiceRequest serviceRequest = new FindUserServiceRequest();
        BeanUtils.copyProperties(request, serviceRequest);

        FindUserServiceResponse findUserServiceResponse = frankmanService.findByUsername(serviceRequest);
        FindUserFacadeResponse findUserFacadeResponse = new FindUserFacadeResponse();
        BeanUtils.copyProperties(findUserServiceResponse, findUserFacadeResponse);

        return findUserFacadeResponse;
    }

    @Override
    public PopulateFacadeResponse populateDb(PopulateFacadeRequest request) throws Exception {

        //Insert Barbiere
        AddBarbiereServiceRequest addBarbiereServiceRequest = new AddBarbiereServiceRequest();
        addBarbiereServiceRequest.setNome("ANTHONY");
        AddBarbiereServiceResponse addBarbiereServiceResponse = frankmanService.addBarbiere(addBarbiereServiceRequest);

        AddBarbiereServiceRequest addBarbiereServiceRequest1 = new AddBarbiereServiceRequest();
        addBarbiereServiceRequest1.setNome("GIORGIONE");
        AddBarbiereServiceResponse addBarbiereServiceResponse1 = frankmanService.addBarbiere(addBarbiereServiceRequest1);

        //Insert Prestazioni
        AddPrestazioneServiceRequest addPrestazioneServiceRequest = new AddPrestazioneServiceRequest();
        addPrestazioneServiceRequest.setDescrizione("TAGLIO CAPELLI");
        AddPrestazioneServiceResponse addPrestazioneServiceResponse = frankmanService.addPrestazione(addPrestazioneServiceRequest);

        AddPrestazioneServiceRequest addPrestazioneServiceRequest1 = new AddPrestazioneServiceRequest();
        addPrestazioneServiceRequest1.setDescrizione("TAGLIO BARBA");
        AddPrestazioneServiceResponse addPrestazioneServiceResponse1 = frankmanService.addPrestazione(addPrestazioneServiceRequest1);

        AddPrestazioneServiceRequest addPrestazioneServiceRequest2 = new AddPrestazioneServiceRequest();
        addPrestazioneServiceRequest2.setDescrizione("SHAMPOO");
        AddPrestazioneServiceResponse addPrestazioneServiceResponse2 = frankmanService.addPrestazione(addPrestazioneServiceRequest2);

        //Insert Prenotazione
        AddPrenotazioneServiceRequest addPrenotazioneServiceRequest = new AddPrenotazioneServiceRequest();
        addPrenotazioneServiceRequest.setId_barbiere(addBarbiereServiceResponse.getBarbiere().getId());
        addPrenotazioneServiceRequest.setId_prestazione(addPrestazioneServiceResponse.getPrestazione().getId());
        frankmanService.addPrenotazione(addPrenotazioneServiceRequest);

        AddPrenotazioneServiceRequest addPrenotazioneServiceRequest1 = new AddPrenotazioneServiceRequest();
        addPrenotazioneServiceRequest1.setId_barbiere(addBarbiereServiceResponse.getBarbiere().getId());
        addPrenotazioneServiceRequest1.setId_prestazione(addPrestazioneServiceResponse1.getPrestazione().getId());
        frankmanService.addPrenotazione(addPrenotazioneServiceRequest1);

        AddPrenotazioneServiceRequest addPrenotazioneServiceRequest2 = new AddPrenotazioneServiceRequest();
        addPrenotazioneServiceRequest2.setId_barbiere(addBarbiereServiceResponse1.getBarbiere().getId());
        addPrenotazioneServiceRequest2.setId_prestazione(addPrestazioneServiceResponse1.getPrestazione().getId());
        frankmanService.addPrenotazione(addPrenotazioneServiceRequest2);

        AddPrenotazioneServiceRequest addPrenotazioneServiceRequest3 = new AddPrenotazioneServiceRequest();
        addPrenotazioneServiceRequest3.setId_barbiere(addBarbiereServiceResponse1.getBarbiere().getId());
        addPrenotazioneServiceRequest3.setId_prestazione(addPrestazioneServiceResponse.getPrestazione().getId());
        frankmanService.addPrenotazione(addPrenotazioneServiceRequest3);

        AddPrenotazioneServiceRequest addPrenotazioneServiceRequest4 = new AddPrenotazioneServiceRequest();
        addPrenotazioneServiceRequest4.setId_barbiere(addBarbiereServiceResponse1.getBarbiere().getId());
        addPrenotazioneServiceRequest4.setId_prestazione(addPrestazioneServiceResponse2.getPrestazione().getId());
        frankmanService.addPrenotazione(addPrenotazioneServiceRequest4);


        return new PopulateFacadeResponse();
    }
}
