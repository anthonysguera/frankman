package it.anthonysguera.webapp.facade.dto.request;

import lombok.Data;

@Data
public class SaveUserFacadeRequest {
    private String nome;
    private String cognome;
    private String email;
    private String phone;
}
