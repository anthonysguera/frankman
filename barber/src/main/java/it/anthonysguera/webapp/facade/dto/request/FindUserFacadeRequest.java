package it.anthonysguera.webapp.facade.dto.request;

import lombok.Data;

@Data
public class FindUserFacadeRequest {

    private String email;

}
