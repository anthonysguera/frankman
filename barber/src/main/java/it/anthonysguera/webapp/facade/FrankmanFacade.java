package it.anthonysguera.webapp.facade;


import it.anthonysguera.webapp.facade.dto.request.FindUserFacadeRequest;
import it.anthonysguera.webapp.facade.dto.request.PopulateFacadeRequest;
import it.anthonysguera.webapp.facade.dto.request.SaveUserFacadeRequest;
import it.anthonysguera.webapp.facade.dto.response.FindUserFacadeResponse;
import it.anthonysguera.webapp.facade.dto.response.PopulateFacadeResponse;
import it.anthonysguera.webapp.facade.dto.response.SaveUserFacadeResponse;

public interface FrankmanFacade {

    SaveUserFacadeResponse save (SaveUserFacadeRequest request) throws Exception;

    FindUserFacadeResponse findByUsername (FindUserFacadeRequest request) throws Exception;

    PopulateFacadeResponse populateDb(PopulateFacadeRequest request) throws Exception;

}
