package it.anthonysguera.webapp.facade.dto.response;

import it.anthonysguera.webapp.domain.User;
import lombok.Data;

@Data
public class SaveUserFacadeResponse {

    private User user;

}
